package com.paperprototype.fm.web.mvc;

import com.paperprototype.fm.dao.EventDao;
import com.paperprototype.fm.dao.MarketDao;
import com.paperprototype.fm.dao.UserDao;
import com.paperprototype.fm.domain.Event;
import com.paperprototype.fm.domain.Market;
import com.paperprototype.fm.domain.User;
import com.paperprototype.fm.web.form.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

@RestController
public class UserController {

    private Logger logger = Logger.getLogger(UserController.class.getName());

    @Autowired
    private UserDao userDao;

    @Autowired
    private EventDao eventDao;

    @Autowired
    private MarketDao marketDao;

    @RequestMapping("/login")
    public User login(@RequestBody AddUserForm addUserForm) {
        logger.info("method invoked.");

        User user = userDao.findByAccessTokenAndAccessType(addUserForm.getAccessToken(), addUserForm.getAccessType());
        if (user != null) {
            return user;
        }
        user = new User();
        user.setAccessToken(addUserForm.getAccessToken());
        user.setAccessType(addUserForm.getAccessType());
        user.setUserType("USER");
        userDao.save(user);

        return userDao.findByAccessTokenAndAccessType(addUserForm.getAccessToken(), addUserForm.getAccessType());
    }

    @RequestMapping(value = "/update-user", produces="text/plain")
    public String updateUser(@RequestBody UpdateUserForm updateUserForm) {
        logger.info("method invoked.");
        User user = userDao.findByAccessTokenAndAccessType(updateUserForm.getAccessToken(), updateUserForm.getAccessType());
        if (user == null) {
            return "FAIL";
        }
        user.setUserType(updateUserForm.getUserType());
        userDao.save(user);

        return "SUCCESS";

    }

    @RequestMapping(value = "/update-vendor-name", produces="text/plain")
    public String updateVendorName(@RequestBody UpdateVendorDetailsForm updateVendorDetailsForm) {
        logger.info("method invoked.");

        User user = userDao.findByVendorName(updateVendorDetailsForm.getVendorName());
        if (user != null && !user.getAccessToken().equals(updateVendorDetailsForm.getAccessToken()) &&
                !user.getAccessType().equals(updateVendorDetailsForm.getAccessType())) {
            return "VENDOR_EXISTS";
        }

        user = userDao.findByAccessTokenAndAccessType(updateVendorDetailsForm.getAccessToken(), updateVendorDetailsForm.getAccessType());
        if (user == null) {
            return "FAIL";
        }

        user.setVendorName(updateVendorDetailsForm.getVendorName());
        userDao.save(user);

        return "SUCCESS";

    }

    @RequestMapping(value = "/update-vendor-desc", produces="text/plain")
    public String updateVendorDescription(@RequestBody UpdateVendorDetailsForm updateVendorDetailsForm) {
        logger.info("method invoked.");

        User user = userDao.findByAccessTokenAndAccessType(updateVendorDetailsForm.getAccessToken(), updateVendorDetailsForm.getAccessType());
        if (user == null) {
            return "FAIL";
        }

        user.setVendorDescription(updateVendorDetailsForm.getVendorDesc());
        userDao.save(user);

        return "SUCCESS";

    }

    @RequestMapping(value = "/update-vendor-website", produces = "text/plain")
    public String updateVendorWebsite(@RequestBody UpdateVendorDetailsForm updateVendorDetailsForm) {
        logger.info("method invoked.");

        User user = userDao.findByAccessTokenAndAccessType(updateVendorDetailsForm.getAccessToken(), updateVendorDetailsForm.getAccessType());
        if (user == null) {
            return "FAIL";
        }

        user.setVendorWebsite(updateVendorDetailsForm.getVendorWebsite());
        userDao.save(user);

        return "SUCCESS";
    }

    @RequestMapping(value = "/subscribe", produces="text/plain")
    public String subscribe(@RequestBody SubscribeForm subscribeForm) {
        logger.info("method invoked.");

        User user = userDao.findByAccessTokenAndAccessType(subscribeForm.getAccessToken(), subscribeForm.getAccessType());
        if (user == null) {
            return "FAIL";
        }

        Set<Event> eventSet = user.getEvents();
        if (subscribeForm.isSubscribed()) {
            boolean alreadySubscribed = false;
            for (Event event : eventSet) {
                if (event.getId() == subscribeForm.getEventId()) {
                    alreadySubscribed = true;
                    break;
                }
            }
            if (!alreadySubscribed) {
                eventSet.add(eventDao.findOne(subscribeForm.getEventId()));
                user.setEvents(eventSet);
                userDao.saveAndFlush(user);
            }
        } else {
            Event eventToRemove = eventDao.findOne(subscribeForm.getEventId());
            Iterator it = eventSet.iterator();
            while (it.hasNext()) {
                Event ev = (Event) it.next();
                if (ev.getId().equals(eventToRemove.getId())) {
                    it.remove();
                    break;
                }
            }
            user.setEvents(eventSet);
            userDao.saveAndFlush(user);
        }

        return "SUCCESS";
    }

    @RequestMapping(value = "/subscribe-market", produces = "text/plain")
    public String subscribe(@RequestBody SubscribeMarketForm subscribeMarketForm) {
        logger.info("method invoked.");

        User user = userDao.findByAccessTokenAndAccessType(subscribeMarketForm.getAccessToken(), subscribeMarketForm.getAccessType());
        if (user == null) {
            return "FAIL";
        }

        Set<Market> marketSet = user.getMarkets();

        if (subscribeMarketForm.isSubscribed()) {
            boolean alreadySubscribed = false;
            for (Market market : marketSet) {
                if (market.getId() == subscribeMarketForm.getMarketId()) {
                    alreadySubscribed = true;
                    break;
                }
            }
            if (!alreadySubscribed) {
                marketSet.add(marketDao.findOne(subscribeMarketForm.getMarketId()));
                user.setMarkets(marketSet);
                user = userDao.saveAndFlush(user);

                // add all events
                Set<Event> events = marketDao.findOne(subscribeMarketForm.getMarketId()).getEvents();
                user.getEvents().addAll(events);
                userDao.save(user);
            }
        } else {
            Market marketToRemove = marketDao.findOne(subscribeMarketForm.getMarketId());
            Iterator it = marketSet.iterator();
            while (it.hasNext()) {
                Market m = (Market) it.next();
                if (m.getId().equals(marketToRemove.getId())) {
                    it.remove();
                    break;
                }
            }
            user.setMarkets(marketSet);
            user = userDao.saveAndFlush(user);

            // remove all events
            Set<Event> events = marketDao.findOne(subscribeMarketForm.getMarketId()).getEvents();
            List<Long> eventIds = new ArrayList<>();
            for (Event event : events) {
                eventIds.add(event.getId());
            }
            Iterator eIt = user.getEvents().iterator();
            while (eIt.hasNext()) {
                Event event = (Event) eIt.next();
                if (eventIds.contains(event.getId())) {
                    eIt.remove();
                }
            }
            userDao.saveAndFlush(user);
        }

        return "SUCCESS";
    }

}
