package com.paperprototype.fm.web.mvc;

import com.google.maps.model.DistanceMatrixRow;
import com.google.maps.model.GeocodingResult;
import com.paperprototype.fm.dao.EventDao;
import com.paperprototype.fm.dao.MarketDao;
import com.paperprototype.fm.dao.UserDao;
import com.paperprototype.fm.domain.Event;
import com.paperprototype.fm.domain.Market;
import com.paperprototype.fm.domain.User;
import com.paperprototype.fm.service.GoogleService;
import com.paperprototype.fm.web.domain.AddEventForm;
import com.paperprototype.fm.web.form.EventSearchForm;
import com.paperprototype.fm.web.form.LocationDetailsForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;
import java.util.logging.Logger;

@RestController
public class EventController {

    private Logger logger = Logger.getLogger(EventController.class.getName());

    @Autowired
    private MarketDao marketDao;

    @Autowired
    private EventDao eventDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private GoogleService googleService;

    @RequestMapping("/add-event")
    public Event addEvent(@RequestBody AddEventForm addEventForm) {
        logger.info("method invoked.");

        logger.info(addEventForm.toString());

        Market market;
        if (addEventForm.getMarketId() == null) {
            if (marketDao.findByNameAndStreetAndCityAndState(addEventForm.getName(), addEventForm.getStreet(), addEventForm.getCity(), addEventForm.getState()) != null) {
                return null;
            }
            market = new Market();
            market.setName(addEventForm.getName());
            market.setStreet(addEventForm.getStreet());
            market.setCity(addEventForm.getCity());
            market.setState(addEventForm.getState());
            market.setZip(addEventForm.getZip());
            try {
                GeocodingResult[] coords = googleService.addressToCoords(addEventForm.fullAddressString());
                market.setLatitude(coords[0].geometry.location.lat);
                market.setLongitude(coords[0].geometry.location.lng);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
            marketDao.save(market);
            market = marketDao.findByNameAndStreetAndCityAndState(market.getName(), market.getStreet(), market.getCity(), market.getState());
        } else {
            market = marketDao.findOne(addEventForm.getMarketId());
        }

        Event event = new Event();
        event.setName(addEventForm.getEventName());
        event.setStartTime(addEventForm.getStartTime());
        event.setEndTime(addEventForm.getEndTime());
        event.setMarket(market);
        event = eventDao.saveAndFlush(event);

        for (User user : market.getUsers()) {
            user.getEvents().add(event);
            userDao.save(user);
        }


        return event;
    }

    @RequestMapping("/events")
    public List<Event> getEvents(@RequestBody EventSearchForm eventSearchForm) {
        List<Event> events;

        if (eventSearchForm.getDate() == null) {
            events = eventDao.findByStartTimeAfter(new Date());
        } else {
            events = eventDao.findByStartTimeBetween(new Date(), eventSearchForm.getDate());
        }

        if (events.size() == 0) return events;
        List<Event> retEvents = new ArrayList<>();
        if (eventSearchForm.getMiles() != null) {
            DistanceMatrixRow row = googleService.distance(eventSearchForm.getLat()+","+eventSearchForm.getLng(), events).rows[0];
            if (row == null) return null;
            for (int i=0;i<row.elements.length;i++) {
                double meters = row.elements[i].distance.inMeters;
                if (getMiles(meters) <= eventSearchForm.getMiles()) {
                    retEvents.add(events.get(i));
                }
            }
            return retEvents;
        } else if (eventSearchForm.getMinutes() != null) {
            DistanceMatrixRow row = googleService.distance(eventSearchForm.getLat()+","+eventSearchForm.getLng(), events).rows[0];
            if (row == null) return null;
            for (int i=0;i<row.elements.length;i++) {
                long seconds = row.elements[i].duration.inSeconds;
                if (getMinutes(seconds) <= eventSearchForm.getMinutes()) {
                    retEvents.add(events.get(i));
                }
            }
            return retEvents;
        }
        return events;
    }

    @RequestMapping("/event/{id}")
    public Event getEvent(@PathVariable("id") long id) {
        return eventDao.findOne(id);
    }

    @RequestMapping("/event/location-details")
    public LocationDetails getLocationDetails(@RequestBody LocationDetailsForm locationDetailsForm) {
        Event event = eventDao.findOne(locationDetailsForm.getEventId());
        List<Event> events = new ArrayList<>(); events.add(event);
        DistanceMatrixRow row = googleService.distance(locationDetailsForm.getLat()+","+locationDetailsForm.getLng(), events).rows[0];
        LocationDetails locationDetails = new LocationDetails();
        if (row == null) return null;
        if (row.elements[0] == null) return null;
        long seconds = row.elements[0].duration.inSeconds;
        double meters = row.elements[0].distance.inMeters;
        locationDetails.setMinutes(String.valueOf(getMinutes(seconds)));
        locationDetails.setMiles(String.valueOf(getMiles(meters)));

        return locationDetails;
    }


    @RequestMapping("/event/{id}/vendors")
    public Set<User> getEventVendors(@PathVariable("id") long id) {
        Event event = eventDao.findOne(id);
        if (event == null) return null;
        return event.getUsers();
    }

    private double getMiles(double i) {
        return i*0.000621371192;
    }

    private long getMinutes(long seconds) {
        return seconds/60;
    }


    public class LocationDetails {
        private String miles;
        private String minutes;

        public String getMiles() {
            return miles;
        }

        public void setMiles(String miles) {
            this.miles = miles;
        }

        public String getMinutes() {
            return minutes;
        }

        public void setMinutes(String minutes) {
            this.minutes = minutes;
        }
    }
}
