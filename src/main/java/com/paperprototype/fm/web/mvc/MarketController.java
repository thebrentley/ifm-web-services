package com.paperprototype.fm.web.mvc;

import com.google.maps.model.GeocodingResult;
import com.paperprototype.fm.dao.EventDao;
import com.paperprototype.fm.dao.MarketDao;
import com.paperprototype.fm.domain.Event;
import com.paperprototype.fm.domain.Market;
import com.paperprototype.fm.service.GoogleService;
import com.paperprototype.fm.web.domain.AddMarketForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

@RestController
public class MarketController {

    private Logger logger = Logger.getLogger(MarketController.class.getName());

    @Autowired
    private MarketDao marketDao;

    @Autowired
    private EventDao eventDao;

    @Autowired
    private GoogleService googleService;

    @RequestMapping("/markets")
    public List<Market> getAllMarkets() {
        logger.info("method invoked.");
        return marketDao.findAllByOrderByName();
    }

    @RequestMapping("/market/{id}")
    public Market getMarket(@PathVariable("id") long id) {
        return marketDao.findOne(id);
    }

    @RequestMapping("/market/{id}/events")
    public List<Event> getMarketEvents(@PathVariable("id") long id) {
        return eventDao.findByStartTimeAfterAndMarket(new Date(), marketDao.getOne(id));
    }

    @RequestMapping("/add-market")
    public String addMarket(AddMarketForm addMarketForm) {
        logger.info("method invoked.");
        if (addMarketForm.getState() == null) addMarketForm.setState("Idaho");
        logger.info(addMarketForm.toString());
        Market market = addMarketForm.toMarket();

        try {
            GeocodingResult[] coords = googleService.addressToCoords(addMarketForm.fullAddressString());
            market.setLatitude(coords[0].geometry.location.lat);
            market.setLongitude(coords[0].geometry.location.lng);
        } catch (Exception e) {
            e.printStackTrace();
            return "FAIL";
        }

        marketDao.save(market);

        return "SUCCESS";
    }


}
