package com.paperprototype.fm.web.form;

import org.apache.commons.lang.builder.ToStringBuilder;

public class UpdateVendorDetailsForm {

    private String accessToken;
    private String accessType;
    private String vendorName;
    private String vendorDesc;
    private String vendorWebsite;

    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAccessType() {
        return accessType;
    }

    public void setAccessType(String accessType) {
        this.accessType = accessType;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getVendorDesc() {
        return vendorDesc;
    }

    public void setVendorDesc(String vendorDesc) {
        this.vendorDesc = vendorDesc;
    }

    public String getVendorWebsite() {
        return vendorWebsite;
    }

    public void setVendorWebsite(String vendorWebsite) {
        this.vendorWebsite = vendorWebsite;
    }
}
