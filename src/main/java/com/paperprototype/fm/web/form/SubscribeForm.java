package com.paperprototype.fm.web.form;

import org.apache.commons.lang.builder.ToStringBuilder;

public class SubscribeForm {

    private String accessToken;
    private String accessType;
    private boolean subscribed;
    private long eventId;

    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAccessType() {
        return accessType;
    }

    public void setAccessType(String accessType) {
        this.accessType = accessType;
    }

    public boolean isSubscribed() {
        return subscribed;
    }

    public void setSubscribed(boolean subscribed) {
        this.subscribed = subscribed;
    }

    public long getEventId() {
        return eventId;
    }

    public void setEventId(long eventId) {
        this.eventId = eventId;
    }
}
