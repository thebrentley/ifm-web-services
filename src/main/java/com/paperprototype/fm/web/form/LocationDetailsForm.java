package com.paperprototype.fm.web.form;

import org.apache.commons.lang.builder.ToStringBuilder;

public class LocationDetailsForm {

    private String lat;
    private String lng;
    private long eventId;

    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public long getEventId() {
        return eventId;
    }

    public void setEventId(long eventId) {
        this.eventId = eventId;
    }
}
