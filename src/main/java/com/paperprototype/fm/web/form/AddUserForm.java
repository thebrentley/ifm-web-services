package com.paperprototype.fm.web.form;

import org.apache.commons.lang.builder.ToStringBuilder;

public class AddUserForm {

    private String accessToken;
    private String accessType;

    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAccessType() {
        return accessType;
    }

    public void setAccessType(String accessType) {
        this.accessType = accessType;
    }
}
