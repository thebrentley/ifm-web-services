package com.paperprototype.fm.web.domain;


import com.paperprototype.fm.domain.Event;
import com.paperprototype.fm.domain.Market;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class AddEventForm {

    private String name;

    private Long marketId;

    private String street;

    private String city;

    private String state;

    private String zip;

    private String eventName;

    @DateTimeFormat(pattern = "MM/dd/yyyy hh:mm a")
    private Date startTime;

    @DateTimeFormat(pattern = "MM/dd/yyyy hh:mm a")
    private Date endTime;

    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public String fullAddressString() {
        return street + " " + city + ", " + state + " " + zip;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getMarketId() {
        return marketId;
    }

    public void setMarketId(Long marketId) {
        this.marketId = marketId;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
}
