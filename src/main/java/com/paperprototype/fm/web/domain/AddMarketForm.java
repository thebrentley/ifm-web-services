package com.paperprototype.fm.web.domain;


import com.paperprototype.fm.domain.Event;
import com.paperprototype.fm.domain.Market;
import org.apache.commons.lang.builder.ToStringBuilder;

public class AddMarketForm {

    private String name;

    private String street;

    private String city;

    private String state;

    private String zip;

    public Market toMarket() {
        Market market = new Market();
        market.setName(name);
        market.setStreet(street);
        market.setCity(city);
        market.setState(state);
        market.setZip(zip);

        return market;
    }

    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public String fullAddressString() {
        return street + " " + city + ", " + state + " " + zip;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }
}
