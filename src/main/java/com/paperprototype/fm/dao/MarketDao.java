package com.paperprototype.fm.dao;

import com.paperprototype.fm.domain.Market;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MarketDao extends JpaRepository<Market, Long> {
    Market findByNameAndStreetAndCityAndState(String name, String street, String city, String state);
    List<Market> findAllByOrderByName();
}
