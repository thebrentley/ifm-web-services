package com.paperprototype.fm.dao;

import com.paperprototype.fm.domain.Event;
import com.paperprototype.fm.domain.Market;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface EventDao extends JpaRepository<Event, Long> {
    List<Event> findByStartTimeBetween(Date now, Date date);
    List<Event> findByStartTimeAfter(Date now);
    List<Event> findByStartTimeAfterAndMarket(Date now, Market market);
}
