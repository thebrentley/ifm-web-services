package com.paperprototype.fm.dao;

import com.paperprototype.fm.domain.Event;
import com.paperprototype.fm.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface UserDao extends JpaRepository<User, Long> {
    User findByAccessTokenAndAccessType(String accessToken, String accessType);
    User findByVendorName(String vendorName);
}
