package com.paperprototype.fm.service;

import com.google.maps.DistanceMatrixApi;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.DistanceMatrix;
import com.google.maps.model.GeocodingResult;
import com.paperprototype.fm.domain.Event;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

@Service
public class GoogleService {

    private final static String APIKEY = "AIzaSyCW-bv49SRietQ-UsQbLuYCEZ0Jq9BUn48";
    private Logger logger = Logger.getLogger(GoogleService.class.getName());

    private GeoApiContext authenticate() {
        GeoApiContext context = new GeoApiContext().setApiKey(APIKEY);
        context.setQueryRateLimit(3).setConnectTimeout(3, TimeUnit.SECONDS)
                .setReadTimeout(2, TimeUnit.SECONDS).setWriteTimeout(2, TimeUnit.SECONDS);
        return context;
    }

    public GeocodingResult[] addressToCoords(String address) {
        GeocodingResult[] results = new GeocodingResult[0];
        try {
            results = GeocodingApi.newRequest(authenticate()).address(address).await();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return results;
    }

    public DistanceMatrix distance(String origin, List<Event> events) {
        logger.info("using " +origin);
        String[] origins = new String[]{
                origin
        };
        String[] destinations = new String[events.size()];
        for (int i=0;i<events.size();i++) {
            destinations[i] = events.get(i).getMarket().getLatitude() + "," + events.get(i).getMarket().getLongitude();
        }
        DistanceMatrix matrix = null;
        try {
            matrix = DistanceMatrixApi.getDistanceMatrix(authenticate(), origins, destinations).await();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return matrix;
    }
}
