package com.paperprototype.fm.config;

import org.hibernate.dialect.MySQL5Dialect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.TransactionManagementConfigurer;

import javax.persistence.EntityManagerFactory;
import java.util.Properties;

@Configuration
@EnableJpaRepositories("com.paperprototype.fm.dao")
@Import(DataSourceConfig.class)
public class PersistenceConfig implements TransactionManagementConfigurer {

    private final static String UNIT_NAME = "mysql";
    private final static String PACKAGE_SCAN = "com.paperprototype.fm.domain";

    @Autowired
    private DataSourceConfig dataSourceConfig;

    @Bean(name = "entityManagerFactory")
    public EntityManagerFactory entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean emf = new LocalContainerEntityManagerFactoryBean();
        emf.setDataSource(dataSourceConfig.dataSource());
        emf.setJpaDialect(new HibernateJpaDialect());
        emf.setJpaVendorAdapter(jpaVendorAdapter());
        emf.setPersistenceUnitName(UNIT_NAME);
        emf.setJpaProperties(getJpaProperties());
        emf.setPackagesToScan(PACKAGE_SCAN);
        emf.afterPropertiesSet();
        return emf.getObject();
    }

    @Bean(name = "jpaVendorAdapter")
    public JpaVendorAdapter jpaVendorAdapter() {
        HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
        jpaVendorAdapter.setShowSql(true);
        jpaVendorAdapter.setDatabase(Database.MYSQL);
        jpaVendorAdapter.setDatabasePlatform(MySQL5Dialect.class.getName());
        jpaVendorAdapter.setGenerateDdl(false);
        return jpaVendorAdapter;
    }

    @Override
    @Bean(name = "transactionManager")
    public PlatformTransactionManager annotationDrivenTransactionManager() {
        JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
        jpaTransactionManager.setEntityManagerFactory(entityManagerFactory());
        return jpaTransactionManager;
    }

    private Properties getJpaProperties() {
        Properties jpaProperties = new Properties();
        jpaProperties.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
        jpaProperties.put("hibernate.show_sql","false");
        jpaProperties.put("hibernate.hbm2ddl.auto", "update");
        jpaProperties.put("hibernate.ejb.naming_strategy", "org.hibernate.cfg.ImprovedNamingStrategy");
        jpaProperties.put("hibernate.cache.provider_class", "org.hibernate.cache.HashtableCacheProvider");
        jpaProperties.put("jadira.usertype.autoRegisterUserTypes", "true");
        jpaProperties.put("jadira.usertype.databaseZone", "jvm");
        jpaProperties.put("hibernate.event.merge.entity_copy_observer", "allow");

        return jpaProperties;
    }
}
