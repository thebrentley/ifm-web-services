package com.paperprototype.fm.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import javax.sql.DataSource;

@Configuration
public class DataSourceConfig {

    @Bean(name = "dataSource")
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://farmersmarket.cgsnekpmlrka.us-west-2.rds.amazonaws.com:3306/FarmersMarket");
        dataSource.setUsername("fmuser");
        dataSource.setPassword("password");

//        dataSource.setUrl("jdbc:mysql://localhost:3306/farmersmarket");
//        dataSource.setUsername("root");
//        dataSource.setPassword("root");

        return dataSource;
    }


}
